# -*- coding: utf-8 -*-
# @Time : 2023/1/11 下午 03:05
# @Author : andy
# @Desc :


from tortoise import fields, Tortoise
from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.models import Model


class WebSite(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=8)


class AdminUser(Model):
    username = fields.CharField(max_length=16, unique=True)
    password = fields.CharField(max_length=200)


class Country(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=8)
    hot = fields.BooleanField(default=False)

    class PydanticMeta:
        max_recursion = 1


class Tags(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=16, null=False, unique=True)
    status = fields.BooleanField(default=True)
    ladies: fields.ManyToManyRelation["Lady"]


class Lady(Model):
    id = fields.BigIntField(pk=True)
    nickname = fields.CharField(max_length=64)
    avatar = fields.CharField(max_length=128, null=True, default='')
    age = fields.IntField(null=False, default=0)
    height = fields.IntField(null=True, default=0)
    weight = fields.IntField(null=True, default=0)
    body_code = fields.CharField(max_length=16, null=True, default='')
    desc = fields.TextField(null=True, default='')
    country = fields.ForeignKeyField("master.Country", related_name="lady")
    image: fields.ReverseRelation['LadyImage']
    category: fields.ManyToManyRelation['Tags'] = fields.ManyToManyField(
        "master.Tags", related_name="ladies", through="lady2tag"
    )

    class Meta:
        table_description = "商品資訊"
        # unique_together = ()

    def __str__(self):
        return self.nickname


class LadyImage(Model):
    id = fields.BigIntField(pk=True)
    storage = fields.CharField(max_length=32)
    lady = fields.ForeignKeyField("master.Lady", related_name="image")


Tortoise.init_models(["models.lady"], "master")

LadySerialize = pydantic_model_creator(
    Lady,
    name='lady_queryset',
    # include=("id", "nickname", "avatar", "age", "height", "weight", "body_code", "desc",),
)

CountrySerialize = pydantic_model_creator(
    Country,
    name='country_queryset',
    include=("id", "title", "hot")
)

TagSerialize = pydantic_model_creator(
    Tags,
    name='tags_queryset',
    include=("id", "title")
)