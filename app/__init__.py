# -*- coding: utf-8 -*-
# @Time : 2022/12/26 下午 05:51
# @Author : andy
# @Desc :

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api.urls import register_router
from dbs.sql import register_database
from utils.exceptions import register_exceptions


def create_app():
    app = FastAPI()
    white_list = [
        '*'
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=white_list,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )
    register_router(app)
    register_database(app)
    register_exceptions(app)
    return app