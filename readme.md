# fastapi搭建一個商品與商品詳情的前後台
## requirements
==需要確認fastapi與jinja2版本問題==
==jinja2版本太高會有錯誤出現==
- fastapi==0.52
- jinja2==3.0.3
- aiofiles==22.1.0
- aiosqlite==0.17.0
- tortoise-orm==0.19.2
- uvicorn==0.20.0

## future
- add admin+template for location and tags crud 