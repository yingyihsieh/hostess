# -*- coding: utf-8 -*-
# @Time : 2023/1/11 下午 02:52
# @Author : andy
# @Desc :
from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise


DB_ORM_CONFIG = {
    "connections": {
        "master": "sqlite://master.db"
    },
    "apps": {
        "master": {
            "models": ["models.lady"],
            "default_connection": "master",
        },
    },
}


def register_database(app: FastAPI):
    # 注册数据库
    register_tortoise(
        app,
        config=DB_ORM_CONFIG,
        generate_schemas=True,
        add_exception_handlers=True,
    )