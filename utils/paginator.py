# -*- coding: utf-8 -*-
# @Time : 2023/1/10 下午 03:42
# @Author : andy
# @Desc :

import math

from tortoise.queryset import QuerySet


async def paginator(pydantic_model,
                    query_set: QuerySet,
                    page: int, size: int,
                    ):
    """分頁器"""

    total = await query_set.count()

    # 通过总数和每页数量计算出总页数
    total_pages = math.ceil(total / size)

    # 分页
    query_set = query_set.offset((page - 1) * size)  # 页数 * 页面大小=偏移量
    query_set = query_set.limit(size)

    data = await pydantic_model.from_queryset(query_set)

    # 生成下一页参数（如果没有下一页则为null）
    next = f"?page={page + 1}&size={size}" if (page + 1) <= total_pages else None
    # 生成上一页参数（如果没有上一页则为null）
    previous = f"?page={page - 1}&size={size}" if (page - 1) >= 1 else None

    return {
        "data": data,
        "total": total,
        "page": page,
        "size": size,
        "total_pages": total_pages,
        "next": next,
        "previous": previous,
    }