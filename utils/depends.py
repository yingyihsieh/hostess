# -*- coding: utf-8 -*-
# @Time : 2023/1/11 下午 04:27
# @Author : andy
# @Desc :

import jwt
from settings import JWT_KEY
from fastapi import Form, File, UploadFile, Query, Header, Cookie
from fastapi.requests import Request
from fastapi.responses import RedirectResponse
from typing import List
from models.lady import Lady, Country, Tags
from utils.exceptions import ErrorException


async def user_model(
        username: str = Form(default=..., min_length=8,max_length=16,regex='[a-zA-Z][a-zA-Z0-9]{7,15}$'),
        password: str = Form(default=..., min_length=8,max_length=32),):
    return {
        'username': username,
        'password': password
    }

async def update_lady_model(
        nickname: str = Form(default=''),
        avatar: UploadFile = File(default=None),
        country: int = Form(default=None),
        age: int = Form(default=0),
        height: int = Form(default=0),
        weight: int = Form(default=0),
        body_code: str = Form(default=''),
        tags: list = Form(default=[]),
        desc: str = Form(default=''),):
    body = dict()
    avatar = await avatar.read()
    if nickname:
        body['nickname'] = nickname
    if avatar:
        body['avatar'] = avatar
    if country:
        country_obj = await Country.filter(id=country).first()
        body['country'] = country_obj
    if age:
        body['age'] = age
    if height:
        body['height'] = height
    if weight:
        body['weight'] = weight
    if body_code:
        body['body_code'] = body_code
    if tags:
        tags = [await Tags.get(id=c) for c in tags]
        body['tags'] = tags
    if desc:
        body['desc'] = desc
    return body


async def verify_ladyCreate(
        nickname: str = Form(...),
        avatar: UploadFile = File(...),
        country: int = Form(...),
        age: int = Form(...),
        height: int = Form(...),
        weight: int = Form(...),
        body_code: str = Form(...),
        tags: list = Form(...),
        desc: str = Form(default=''),):

    check = await Lady.filter(nickname=nickname, country=country).exists()
    if check:
        raise ErrorException(msg='數據已存在')
    country_obj = await Country.filter(id=country).first()
    if not country_obj:
        raise ErrorException(msg='國家不存在')
    tags = [await Tags.get(title=c) for c in tags]

    return {
        'nickname': nickname,
        'country': country_obj,
        'age': age,
        'avatar': avatar,
        'height': height,
        'weight': weight,
        'body_code': body_code,
        'tags': tags,
        'desc': desc,
    }


async def images_model(thumb: List[int] = Form(default=[]),):
    return thumb


async def verify_token(request: Request, auth: str = Cookie(default=None)):
    if auth is None:
        print('auth=',auth)
        resp = RedirectResponse(url=request.url_for('login_page'))
        resp.status_code = 302
        return resp
    try:
        payload = jwt.decode(auth, JWT_KEY, algorithms=['HS256'])
    except jwt.ExpiredSignatureError:
        resp = RedirectResponse(url=request.url_for('login_page'))
        resp.status_code = 302
        return resp
    except jwt.DecodeError:
        resp = RedirectResponse(url=request.url_for('login_page'))
        resp.status_code = 302
        return resp
    except jwt.InvalidTokenError:
        resp = RedirectResponse(url=request.url_for('login_page'))
        resp.status_code = 302
        return resp
    return payload