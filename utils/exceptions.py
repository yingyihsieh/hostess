# -*- coding: utf-8 -*-
# @Time : 2023/1/11 下午 04:11
# @Author : andy
# @Desc :

from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse


class BaseException(Exception):
    def __init__(self, msg: str):
        self.msg = msg


class ErrorException(BaseException):
    pass


def register_exceptions(app: FastAPI):
    @app.exception_handler(ErrorException)
    def self_exception_handler(request: Request, exc: ErrorException):
        return JSONResponse(
            status_code=403,
            content={'code': -1, 'data': None, 'msg': f'{exc.msg}'},
        )