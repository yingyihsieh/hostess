# -*- coding: utf-8 -*-
# @Time : 2023/1/11 下午 04:03
# @Author : andy
# @Desc :

from fastapi.responses import JSONResponse


def success_response(data, msg):
    return JSONResponse(
        status_code=200,
        content={'code': 1, 'data': data, 'msg': msg}
    )


def fail_response(data, msg):
    return JSONResponse(
        status_code=400,
        content={'code': -1, 'data': data, 'msg': msg}
    )
