# -*- coding: utf-8 -*-
# @Time : 2023/1/31 下午 09:02
# @Author : andy
# @Desc :

import jwt
import datetime
from settings import JWT_KEY, JWT_EXPIRE


def create_token(user):
    headers = {
        'type': 'jwt',
        'alg': 'HS256'
    }

    payload = {
        'id': user.id,
        'username': user.username,
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=JWT_EXPIRE)
    }

    token = jwt.encode(headers=headers, key=JWT_KEY, payload=payload, algorithm='HS256')
    return token


