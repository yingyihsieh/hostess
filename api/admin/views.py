# -*- coding: utf-8 -*-
# @Time : 2022/12/26 下午 06:16
# @Author : andy
# @Desc :


import os.path
import time

import aiofiles
from typing import Union, Any
from fastapi import APIRouter, Depends, Form, File, UploadFile, Query, Path
from fastapi.responses import RedirectResponse, Response
from starlette.templating import Jinja2Templates
from starlette.requests import Request
from settings import IMAGE
from models.lady import *
from utils.responses import success_response, fail_response
from utils.exceptions import ErrorException
from utils.depends import verify_ladyCreate, update_lady_model, images_model, user_model, verify_token
from utils.paginator import paginator
from utils.auth import create_token
from passlib.hash import pbkdf2_sha256

router = APIRouter()
templates = Jinja2Templates(directory='templates')


@router.get('/register_form')
async def register_page(request: Request, ):
    '''註冊頁面'''

    return templates.TemplateResponse(
        'adminRegister.html',
        {
            'request': request,
        }
    )


@router.post('/register')
async def admin_register(request: Request, body: dict = Depends(user_model)):
    '''註冊API'''
    body['password'] = pbkdf2_sha256.hash(body['password'])
    obj, created = await AdminUser.get_or_create(username=body['username'], defaults={'password': body['password']})
    if not created:
        return {'msg': 'exist'}
    response = RedirectResponse(url=request.url_for('login_page'))
    response.status_code = 302
    return response


@router.get('')
async def login_page(request: Request, ):
    '''後台首頁 登入頁面'''
    web = await WebSite.first()
    return templates.TemplateResponse(
        'adminLogin.html',
        {
            'request': request,
            'web_title': web.title
        }
    )


@router.post('/login')
async def admin_login(request: Request,
                      body: dict = Depends(user_model)
                      ):
    '''登入API'''

    user = await AdminUser.filter(username=body['username']).first()
    if not user:
        return
    check = pbkdf2_sha256.verify(body['password'], user.password)
    if not check:
        return
    token = create_token(user)
    response = RedirectResponse(url=request.url_for('admin_index'))
    response.set_cookie('auth', token)
    response.status_code = 302
    return response


@router.get('/index')
async def admin_index(request: Request,
                      page: int = Query(default=1),
                      size: int = Query(default=12),
                      user_info: Union[dict, Any] = Depends(verify_token),
                      ):
    '''登入後台首頁 需要token權限'''

    if not isinstance(user_info, dict):
        return user_info
    data = Lady.all()
    data = await paginator(pydantic_model=LadySerialize,
                           query_set=data,
                           page=page,
                           size=size)

    return templates.TemplateResponse(
        'admin.html',
        {
            'request': request,
            'data': data,
        },
    )


@router.post('/country/create')
async def country_create(request: Request, title: str = Form(...), hot: int = Form(...)):
    try:
        obj = await Country.filter(title=title).first()
        if obj:
            return fail_response(data=None, msg='數據已存在')
        if hot:
            await Country.create(title=title, hot=True)
        else:
            await Country.create(title=title, hot=False)
        response = RedirectResponse(url=request.url_for('admin_others'))
        response.status_code = 302
        return response
    except:
        raise ErrorException(msg='db插入數據失敗')


@router.post('/service/create')
async def service_create(title: str = Query(...)):
    try:
        obj = await Tags.filter(title=title).first()
        if obj:
            return fail_response(data=None, msg='數據已存在')
        await Tags.create(title=title)
        return success_response(data=None, msg='創建成功')
    except:
        raise ErrorException(msg='db插入數據失敗')


@router.get('/lady/addpage')
async def lady_add_page(request: Request):
    ''' 後台新增女優頁面 '''
    countries = await CountrySerialize.from_queryset(Country.all())
    services = await TagSerialize.from_queryset(Tags.all())

    return templates.TemplateResponse(
        'ladyCreate.html',
        {
            'request': request,
            'services': services,
            'countries': countries,
        }
    )


@router.post('/lady/create')
async def lady_create(request: Request, body: dict = Depends(verify_ladyCreate)):
    file_dir = os.path.join(IMAGE, f'{body["nickname"]}_{body["country"].title}')
    if not os.path.exists(file_dir):
        os.mkdir(file_dir)
    file_path = os.path.join(file_dir, 'avatar.jpg')

    async with aiofiles.open(file_path, "wb") as f1:
        try:
            c = await body['avatar'].read()
            await f1.write(c)
        except:
            raise ErrorException(msg='上傳失敗')
    body['avatar'] = '/'.join(file_path.replace('\\', '/').rsplit('/', 3)[1:])
    lady_obj = await Lady.create(**body)
    await lady_obj.category.add(*body['tags'])
    response = RedirectResponse(url=request.url_for('admin_index'))
    response.status_code = 302
    return response


@router.get('/lady/delete')
async def delete_lady(
        request: Request,
        id: int = Query(...),
):
    obj = await Lady.filter(id=id).exists()
    if obj:
        await Lady.filter(id=id).delete()
        response = RedirectResponse(url=request.url_for('admin_index'))
        response.status_code = 302
        return response


@router.get('/lady/editinfo')
async def lady_edit(request: Request, pk: int = Query(...)):
    data = await LadySerialize.from_queryset_single(Lady.filter(id=pk).first())
    data.category = [d.title for d in data.category]
    services = await Tags.all().values("id", "title")
    countries = await Country.all().values("id", "title")
    return templates.TemplateResponse(
        'ladyEdit.html',
        {
            'request': request,
            'services': services,
            'countries': countries,
            'data': data,
        }
    )


@router.get('/lady/img/{pk}')
async def image_info(
        request: Request,
        pk: int = Path(...),
):
    lady = await Lady.filter(id=pk).first()
    if not lady:
        return {'msg': '女優不存在'}
    imgs = await lady.image

    return templates.TemplateResponse(
        'Imginfo.html',
        {
            'request': request,
            'imgs': imgs,
            'nickname': lady.nickname,
            'uid': pk,
        }
    )


@router.post('/lady/update/{pk}')
async def lady_update(
        request: Request,
        pk: int = Path(...),
        body: dict = Depends(update_lady_model),
):
    '''後台更新女優'''

    target = await Lady.filter(id=pk).first()
    if not target:
        return {'msg': '女優不存在'}

    if 'avatar' in body:
        if body['avatar'].filename.split('.')[-1].lower() not in {'jpg', 'jpeg', 'png'}:
            return {'msg': '請上傳圖片'}
        path_params = target.avatar.split('/')
        file_name = f'{path_params[1]}/{int(time.time() * 1000)}.jpg'
        file_path = os.path.join(IMAGE, file_name)
        async with aiofiles.open(file_path, "wb") as f1:
            try:
                await f1.write(body['avatar'])
            except Exception as e:
                return {'msg': f'upload failed==>{e}'}
        body['avatar'] = '/'.join(file_path.replace('\\', '/').rsplit('/', 3)[1:])
    await Lady.filter(id=pk).update(**body)
    response = RedirectResponse(url=request.url_for('admin_index'))
    response.status_code = 302
    return response


@router.post('/photo/upload/{pk}')
async def imgs_upload(request: Request,
                      pk: int = Path(...),
                      files: UploadFile = File(...),
                      ):
    target = await Lady.filter(id=pk).first()
    if not target:
        return {'msg': '女優不存在'}
    path_params = target.avatar.split('/')
    file_name = f'{path_params[1]}/{int(time.time() * 1000)}.jpg'
    file_path = os.path.join(IMAGE, file_name)

    async with aiofiles.open(file_path, "wb") as f1:
        try:
            c = await files.read()
            await f1.write(c)
        except Exception as e:
            return {'msg': f'upload failed==>{e}'}
    storage = '/'.join(file_path.replace('\\', '/').rsplit('/', 3)[1:])
    await LadyImage.create(storage=storage, lady=target)
    response = RedirectResponse(url=request.url_for('image_info', pk=pk))
    response.status_code = 302
    return response


@router.post('/img/del')
async def image_delete(request: Request, lady: int = Query(...), imgs: list = Depends(images_model)):
    await LadyImage.filter(id__in=imgs).delete()
    response = RedirectResponse(url=request.url_for('image_info', pk=lady))
    response.status_code = 302
    return response


@router.post('/web_title/edit')
async def create_web_title(request: Request, title: str = Form(...)):
    obj = await WebSite.first()
    if not obj:
        await WebSite.create(title=title)
    else:
        obj.title = title
        await obj.save()
    response = RedirectResponse(url=request.url_for('admin_others'))
    response.status_code = 302
    return response


@router.get('/other_settings')
async def admin_others(request: Request,
                       page: int = Query(default=1),
                       size: int = Query(default=10),
                       user_info: Union[dict, Any] = Depends(verify_token),
                       ):
    if not isinstance(user_info, dict):
        return user_info
    data = Country.all()
    data = await paginator(pydantic_model=CountrySerialize,
                           query_set=data,
                           page=page,
                           size=size)

    web = await WebSite.first()
    return templates.TemplateResponse(
        'otherSetting.html',
        {
            'request': request,
            'data': data,
            'web_title': web.title
        },
    )


@router.post('/country/update')
async def country_update(request: Request,
                         pk: int = Query(...),
                         title: str = Form(default=''),
                         hot: str = Form(default='')
                         ):
    body = dict()
    if title:
        body['title'] = title
    if hot:
        if int(hot):
            body['hot'] = True
        else:
            body['hot'] = False
    if body:
        await Country.filter(id=pk).update(**body)
    response = RedirectResponse(url=request.url_for('admin_others'))
    response.status_code = 302
    return response


@router.get('/country/edit')
async def country_edit(request: Request,
                       pk: int = Query(...),
                       action: str = Query(...)
                       ):
    if action == 'del':
        await Country.filter(id=pk).delete()
        response = RedirectResponse(url=request.url_for('admin_others'))
        response.status_code = 302
        return response
    if action == 'update':
        data = await Country.filter(id=pk).first().values("id", "title", "hot")
        return templates.TemplateResponse(
            'updateCountry.html',
            {
                'request': request,
                'data': data,
            },
        )
    else:
        raise fail_response(msg='不正常要求')
