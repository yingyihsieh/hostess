# -*- coding: utf-8 -*-
# @Time : 2022/12/26 下午 06:24
# @Author : andy
# @Desc :

from fastapi import FastAPI
from .admin.views import router as admin_urls
from .app1.views import router as app_urls

def register_router(app: FastAPI):
    app.include_router(admin_urls, prefix='/admin')
    app.include_router(app_urls, prefix='/fishtea')