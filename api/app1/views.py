# -*- coding: utf-8 -*-
# @Time : 2022/12/26 下午 10:46
# @Author : andy
# @Desc :

from fastapi import APIRouter, Query, Depends
from starlette.requests import Request
from starlette.templating import Jinja2Templates
from models.lady import *
from utils.paginator import paginator

router = APIRouter()
templates = Jinja2Templates(directory='templates')


async def get_bars():
    web = await WebSite.first()
    country = await Country.filter(hot=True).values("id", "title")
    return {
        'web_title': web.title,
        'hot_countries': country
    }


@router.get('')
async def home_page(
        request: Request,
        area: int = Query(default=''),
        page: int = Query(default=1),
        size: int = Query(default=8),
        ):
    if area:
        data = Lady.filter(country_id=area)
    else:
        data = Lady.all()
    data = await paginator(pydantic_model=LadySerialize,
                           query_set=data,
                           page=page,
                           size=size)
    hot_area = await Country.filter(hot=True).values("id", "title")
    return templates.TemplateResponse('home.html', {
        'request': request,
        'data': data,
        'bar':await get_bars(),
        'area': area
    })


@router.get('/detail/{pk}')
async def lady_detail(request: Request, pk: int):
    data = await LadySerialize.from_queryset_single(Lady.filter(id=pk).first())
    print(data)

    # return data
    return templates.TemplateResponse('productDetail.html', {
        'request': request,
        'data': data,
        'bar': await get_bars(),
    })