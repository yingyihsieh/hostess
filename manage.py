# -*- coding: utf-8 -*-
# @Time : 2022/12/26 下午 05:50
# @Author : andy
# @Desc :

import uvicorn
from app import create_app
from fastapi.staticfiles import StaticFiles


server = create_app()
server.mount("/static", StaticFiles(directory="static"), name="static")


if __name__ == '__main__':
    uvicorn.run(app='manage:server', host="0.0.0.0", port=5000, reload=True)